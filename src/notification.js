const PushBullet = require('pushbullet');
const {
  pushbulletAccessTokens,
  telegramToken,
  telegramChatId,
} = require('../config');

const Telegraf = require('telegraf');

const bot = new Telegraf(telegramToken);

const pusher = new PushBullet(pushbulletAccessTokens);

async function pushToTelegram(apartment) {
  const messageId = await bot.telegram.sendMessage(telegramChatId, `כתובת: ${apartment.address}
  מחיר: ${apartment.price}
  חדרים: ${apartment.rooms}
  תאריך כניסה: ${apartment.enterDate}
  קומה: ${apartment.floor}
  תאריך פרסום: ${apartment.publishDate}
  קישור: ${apartment.adUrl}
  שכונה: ${apartment.neighborhood}`).then((message) => (message.message_id)).catch((e) => {
    console.log(e);
  });
  if (apartment.imagesArr && apartment.imagesArr.length > 0) {
    const imagesMedia = apartment.imagesArr.map((item) => {
      return {
        media: item,
        type: 'photo',
      };
    });
    bot.telegram.sendMediaGroup(telegramChatId, imagesMedia, {
      reply_to_message_id: messageId,
    }).catch((e) => {
      console.log(e);
    });
  }
  console.log('sent to telegram');
}

async function pushToAll(title, body) {
  return new Promise((res, rej) => {
    pusher.note(null, title, body, (error, response) => {
      if (error) {
        rej(error);
        return;
      }
      res(response);
    });
  });
}

module.exports = {
  pushToAll,
  pushToTelegram,
};