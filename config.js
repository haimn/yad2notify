module.exports = {
  url: process.env.YAD2_URL,
  pushbulletAccessTokens: process.env.PUSHBULLET_TOKEN,
  telegramToken: process.env.TELEGRAM_TOKEN,
};
