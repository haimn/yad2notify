const yad2scraper = require('../src/yad2scraper');
const scrape = yad2scraper.scrape;
const scrapeImages = yad2scraper.scrapeImages;
const {
  urls
} = require('../config');
const {
  writeData,
  readData
} = require('../src/presistentDataStore');
const notification = require('../src/notification');
const pushToAll = notification.pushToAll;
const pushToTelegram = notification.pushToTelegram;

console.log('\nyad2notify starting\n');
const oldData = readData();

async function mainLoop() {
  for (urlObj of urls) {
  // urls.forEach(async (urlObj) => {
    const url = urlObj.url;
    const newData = await scrape({
      url,
    });
    for (x of Object.keys(newData)) {
    // Object.keys(newData).forEach(async (x) => {
      if (!oldData[x]) {
        oldData[x] = newData[x];
        const id = x.substring(x.lastIndexOf('_') + 1);
        const type = x.substr(x.lastIndexOf('_') - 1, 1);
        const imagesUrl = 'http://www.yad2.co.il/Nadlan/ViewImage.php?CatID=2&SubCatID=2&RecordID=' + id;
        const dataArr = newData[x].split('\t \t');
        if (type == '2' && (!urlObj.streets || urlObj.streets.indexOf(dataArr[3]) > -1)) {
          const imagesArr = await scrapeImages(imagesUrl);
          console.log(`images length: ${imagesArr.length}`);
          pushToTelegram({
            address: dataArr[3],
            price: dataArr[4],
            rooms: dataArr[5],
            enterDate: dataArr[6],
            floor: dataArr[7],
            publishDate: dataArr[9].replace('\t  ', ''),
            adUrl: 'http://www.yad2.co.il/Nadlan/rent_info.php?NadlanID=' + id,
            imagesArr,
            neighborhood: urlObj.neighborhood,
          });
        }
      }
    // });
  // });
    }
  }
  writeData(oldData);
  console.log('taking a long nap\n');
}

mainLoop();
setInterval(() => mainLoop(), 60 * 60 * 1000);